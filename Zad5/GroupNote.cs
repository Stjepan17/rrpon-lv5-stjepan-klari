﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad5
{
    class GroupNote : Note
    {
        private List<String> peopleNames;
        public GroupNote(string message, ITheme theme) : base(message, theme) {
        peopleNames = new List<String>();
        }
        public void AddPeople(String name)
        {
            peopleNames.Add(name);
        }
        public void DeletePeople(String name)
        {
            peopleNames.Remove(name);
        }
        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("REMINDER: ");
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            foreach (String a in peopleNames)
            {
                this.ChangeColor();
                Console.WriteLine(a);
            }
            Console.ResetColor();
        }
    }
}
