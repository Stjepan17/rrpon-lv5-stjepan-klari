﻿using System;

namespace Zad5
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme theme1 = new LightTheme();
            ITheme theme2 = new SmoothTheme();
            ReminderNote note1 = new ReminderNote("Text1", theme1);
            ReminderNote note2 = new ReminderNote("Text2", theme2);
            note1.Show();
            Console.WriteLine();
            note2.Show();
            Console.WriteLine();
            GroupNote note3 = new GroupNote("List of good people:", theme2);
            note3.AddPeople("Stjepan");
            note3.AddPeople("Marko");
            note3.AddPeople("Marijan");
            note3.AddPeople("Robert");
            note3.DeletePeople("Marko");
            note3.Show();
            Console.WriteLine();
            Notebook notes = new Notebook(theme2);
            notes.AddNote(note1);
            notes.AddNote(note2);
            notes.Display();
            notes.ChangeTheme(theme1);
            notes.Display();
        }
    }
}
