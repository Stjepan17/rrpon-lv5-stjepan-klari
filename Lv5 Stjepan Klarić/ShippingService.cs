﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv5_Stjepan_Klarić
{
    class ShippingService
    {
        private double uniquePrice;
        public ShippingService(double uniquePrice) {
            this.uniquePrice = uniquePrice;
        }
        public double calculatePrice(IShipable objekt) {
            return uniquePrice * objekt.Weight;
        }
       
    }
}
