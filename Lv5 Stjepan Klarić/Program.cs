﻿using System;
using System.Collections.Generic;
namespace Lv5_Stjepan_Klarić
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box("kutija");
            Product produkt1 = new Product("lopta", 50, 1);
            Product produkt2 = new Product("knjiga", 45, 0.7);
            box.Add(produkt1);
            box.Add(produkt2);
            ShippingService ship = new ShippingService(4);
            Console.WriteLine(ship.calculatePrice(box));
        }
    }
}
