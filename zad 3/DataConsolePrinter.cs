﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zad_3
{
    class DataConsolePrinter
    {
        public void Print(IDataset data)
        {
            if (data.GetData() == null)
            {
                Console.WriteLine("There is nothing to read!");
                return;
            }
            foreach (List<string> lines in data.GetData())
            {
                foreach (string line in lines)
                {
                    Console.Write(line + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
