﻿using System;

namespace zad_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("primjer.txt");
            User user1 = User.GenerateUser("Marko");
            User user2 = User.GenerateUser("Marko");
            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(user2);
            VirtualProxyDataset proxy3 = new VirtualProxyDataset("primjer.txt");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.Print(proxy1);
            Console.WriteLine();
            printer.Print(proxy2);
            Console.WriteLine();
            printer.Print(proxy3);
        }
    }
}
